package pl.stepinski.marketpricehandler.prices.controllers;

import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.stepinski.marketpricehandler.prices.model.Price;
import pl.stepinski.marketpricehandler.prices.repositories.PriceRepository;
import pl.stepinski.marketpricehandler.prices.services.PriceService;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@AllArgsConstructor
public class PricesController {

    private static final Logger logger = LogManager.getLogger(PricesController.class);
    private final PriceRepository priceRepository;
    private final PriceService priceService;

    @GetMapping(path = "prices")
    public List<Price> getPrices() {
        logger.info("Get prices");
        return priceService.getPrices();
    }

    @GetMapping(path = "price")
    public Price getPrice(@PathParam("instrumentName") String instrumentName) {
        logger.info("Get getPriceByInstrumentName: " + instrumentName);
        return priceService.getPriceByInstrumentName(instrumentName);
    }

    @GetMapping(path = "prices/mockMessage")
    public void mockMessage(@PathParam("message") String message) {
        logger.info("Mocking incoming message");
        priceService.onMessage(message);
    }
}
