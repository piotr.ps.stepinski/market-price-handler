package pl.stepinski.marketpricehandler.prices.comparators;

import pl.stepinski.marketpricehandler.prices.model.Price;

import java.time.LocalDateTime;
import java.util.Comparator;

public class PriceTimestampComparator implements Comparator<Price> {

    @Override
    public int compare(Price o1, Price o2) {
        LocalDateTime timestamp1 = o1.getTimestamp();
        LocalDateTime timestamp2 = o2.getTimestamp();
        return timestamp1.compareTo(timestamp2);
    }

}