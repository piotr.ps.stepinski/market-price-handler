package pl.stepinski.marketpricehandler.prices.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.stepinski.marketpricehandler.prices.model.Price;

public interface PriceRepository extends JpaRepository<Price, Long> {
    Price findByInstrumentName(String instrumentName);
    void deleteByInstrumentNameIn(Iterable<String> instrumentName);
}
