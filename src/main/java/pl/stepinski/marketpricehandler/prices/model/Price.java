package pl.stepinski.marketpricehandler.prices.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@EqualsAndHashCode
@Builder
@ToString
@Entity
@Table(name = "price")
public class Price {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "instrument_name")
    private String instrumentName;

    @Column(name = "bid")
    private BigDecimal bid;

    @Column(name = "ask")
    private BigDecimal ask;

    @Column(name = "timestamp")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime timestamp;

    public Price addBidCommission(BigDecimal commission) {
        bid = addCommission(bid, commission);
        return this;
    }

    public Price addAskCommission(BigDecimal commission) {
        ask = addCommission(ask, commission);
        return this;
    }

    private static BigDecimal addCommission(BigDecimal value, BigDecimal commission) {
        return value.multiply(BigDecimal.ONE.add(commission));
    }
}
