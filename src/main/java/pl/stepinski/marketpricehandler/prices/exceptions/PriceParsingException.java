package pl.stepinski.marketpricehandler.prices.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PriceParsingException extends RuntimeException {
    private Exception cause;
}
