package pl.stepinski.marketpricehandler.prices.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.stepinski.marketpricehandler.prices.jms.PriceFeedHandler;
import pl.stepinski.marketpricehandler.prices.model.Price;
import pl.stepinski.marketpricehandler.prices.parsers.PriceParser;
import pl.stepinski.marketpricehandler.prices.repositories.PriceRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@Service
public class PriceService implements PriceFeedHandler {

    private final static BigDecimal BID_COMMISSION = new BigDecimal("-0.1");

    private final static BigDecimal ASK_COMMISSION = new BigDecimal("0.1");

    private final PriceRepository priceRepository;

    public List<Price> getPrices() {
        return priceRepository.findAll();
    }

    public Price getPriceByInstrumentName(String instrumentName) {
        return priceRepository.findByInstrumentName(instrumentName);
    }

    @Transactional
    public void onMessage(String message) {

        Map<String, Optional<Price>> parsedPrices = PriceParser.parseMultiplePrices(message);

        List<Price> pricesWithCommissions = parsedPrices.values().stream()
                .map(Optional::get)
                .map(p -> p.addBidCommission(BID_COMMISSION))
                .map(p -> p.addAskCommission(ASK_COMMISSION))
                .toList();

        Set<String> instrumentNames = parsedPrices.keySet();

        priceRepository.deleteByInstrumentNameIn(instrumentNames);
        priceRepository.flush();
        priceRepository.saveAll(pricesWithCommissions);
    }
}
