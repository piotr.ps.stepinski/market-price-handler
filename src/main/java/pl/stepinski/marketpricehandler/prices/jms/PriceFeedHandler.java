package pl.stepinski.marketpricehandler.prices.jms;

public interface PriceFeedHandler {
    void onMessage(String message);
}
