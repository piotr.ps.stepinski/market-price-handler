package pl.stepinski.marketpricehandler.prices.parsers;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.stepinski.marketpricehandler.prices.comparators.PriceTimestampComparator;
import pl.stepinski.marketpricehandler.prices.exceptions.PriceListParsingException;
import pl.stepinski.marketpricehandler.prices.exceptions.PriceParsingException;
import pl.stepinski.marketpricehandler.prices.model.Price;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class PriceParser {

    private final static PriceTimestampComparator priceTimestampComparator = new PriceTimestampComparator();

    public static Map<String, Optional<Price>> parseMultiplePrices(String message) throws PriceListParsingException {
        try {
            Map<String, Optional<Price>> priceMap = Arrays.stream(message.split("\n"))
                    .map(PriceParser::parseSinglePrice)
                    .collect(Collectors.groupingBy(Price::getInstrumentName, Collectors.maxBy(priceTimestampComparator)));

            return priceMap;
        } catch (Exception exception) {
            throw new PriceListParsingException(exception);
        }
    }

    public static Price parseSinglePrice(String line) throws PriceParsingException {

        try {
            String[] tokens = line.split(",");

            Price price = Price.builder()
                    .id(Long.parseLong(tokens[0].trim()))
                    .instrumentName(tokens[1].trim())
                    .bid(new BigDecimal(tokens[2].trim()))
                    .ask(new BigDecimal(tokens[3].trim()))
                    .timestamp(parseDateTime(tokens[4].trim()))
                    .build();
            return price;
        } catch (Exception exception) {
            throw new PriceParsingException(exception);
        }
    }

    public static LocalDateTime parseDateTime(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss:SSS");
        return LocalDateTime.parse(dateString, formatter);
    }
}
