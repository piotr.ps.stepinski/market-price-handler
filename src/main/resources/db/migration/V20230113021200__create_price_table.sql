CREATE TABLE price (
    id bigint PRIMARY KEY,
    instrument_name varchar,
    bid numeric(18,4),
    ask numeric(18,4),
    timestamp timestamp
);