package pl.stepinski.marketpricehandler.prices.comparators;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import pl.stepinski.marketpricehandler.prices.model.Price;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class PriceTimestampComparatorTest {

    PriceTimestampComparator priceTimestampComparator = new PriceTimestampComparator();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss:SSS");
    Price priceWithEarlyDate = new Price(108L, "GBP/USD", new BigDecimal("1.2500"), new BigDecimal("1.2560"), LocalDateTime.parse("01-06-2020 12:01:02:002", formatter));
    Price priceWithLateDate = new Price(109L, "GBP/USD", new BigDecimal("1.2499"), new BigDecimal("1.2561"), LocalDateTime.parse("01-06-2020 12:01:02:100", formatter));

    @Test
    void shouldGetLatestPriceFormPricesListInOrder() {
        List<Price> prices = new ArrayList<>();
        prices.add(priceWithEarlyDate);
        prices.add(priceWithLateDate);

        Price latestPrice = prices.stream().max(priceTimestampComparator).get();

        assertThat(latestPrice).isEqualTo(priceWithLateDate);
    }

    @Test
    void shouldGetLatestPriceFormPricesListOutOfOrder() {
        List<Price> prices = new ArrayList<>();
        prices.add(priceWithLateDate);
        prices.add(priceWithEarlyDate);

        Price latestPrice = prices.stream().max(priceTimestampComparator).get();

        assertThat(latestPrice).isEqualTo(priceWithLateDate);
    }
}
