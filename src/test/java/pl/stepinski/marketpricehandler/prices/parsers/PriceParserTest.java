package pl.stepinski.marketpricehandler.prices.parsers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import pl.stepinski.marketpricehandler.prices.comparators.PriceTimestampComparator;
import pl.stepinski.marketpricehandler.prices.exceptions.PriceListParsingException;
import pl.stepinski.marketpricehandler.prices.exceptions.PriceParsingException;
import pl.stepinski.marketpricehandler.prices.model.Price;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@SpringBootTest
public class PriceParserTest {
    String invalidPriceMessage = "INVALID";
    String messageWithPrice = "106, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001";
    String messageWithPricesInOrder = """
            106, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001
            108, GBP/USD, 1.2500,1.2560,01-06-2020 12:01:02:002
            109, GBP/USD, 1.2499,1.2561,01-06-2020 12:01:02:100""";

    String messageWithPricesOutOfOrder = """
            106, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001
            109, GBP/USD, 1.2499,1.2561,01-06-2020 12:01:02:100
            108, GBP/USD, 1.2500,1.2560,01-06-2020 12:01:02:002
            """;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss:SSS");

    Price price106 = new Price(106L, "EUR/USD", new BigDecimal("1.1000"), new BigDecimal("1.2000"), LocalDateTime.parse("01-06-2020 12:01:01:001", formatter));
    Price price108 = new Price(108L, "GBP/USD", new BigDecimal("1.2500"), new BigDecimal("1.2560"), LocalDateTime.parse("01-06-2020 12:01:02:002", formatter));
    Price price109 = new Price(109L, "GBP/USD", new BigDecimal("1.2499"), new BigDecimal("1.2561"), LocalDateTime.parse("01-06-2020 12:01:02:100", formatter));

    @Test
    void shouldCatchPriceListParsingException() {
        Assertions.assertThrows(PriceListParsingException.class, () -> {
            PriceParser.parseMultiplePrices(invalidPriceMessage);
        }, "PriceListParsingException was expected");
    }

    @Test
    void shouldCatchPriceParsingException() {
        Assertions.assertThrows(PriceParsingException.class, () -> {
            PriceParser.parseSinglePrice(invalidPriceMessage);
        }, "PriceParsingException was expected");
    }

    @Test
    void shouldParseMessageWithPrice() {
        Price parsedPrice = PriceParser.parseSinglePrice(messageWithPrice);
        assertThat(parsedPrice).isEqualTo(price106);
    }

    @Test
    void shouldParseMessageWithPricesListInOrder() {
        Map<String, Optional<Price>> parsedPrices = PriceParser.parseMultiplePrices(messageWithPricesInOrder);

        assertThat(parsedPrices.values().size()).isEqualTo(2);
        assertThat(parsedPrices.get("EUR/USD").get()).isEqualTo(price106);
        assertThat(parsedPrices.get("GBP/USD").get()).isEqualTo(price109);
    }

    @Test
    void shouldParseMessageWithPricesListOutOfOrder() {
        Map<String, Optional<Price>> parsedPrices = PriceParser.parseMultiplePrices(messageWithPricesOutOfOrder);

        assertThat(parsedPrices.values().size()).isEqualTo(2);
        assertThat(parsedPrices.get("EUR/USD").get()).isEqualTo(price106);
        assertThat(parsedPrices.get("GBP/USD").get()).isEqualTo(price109);
    }
}
