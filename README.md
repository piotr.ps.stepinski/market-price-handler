# Project description

Project for recruitment purposes.

# Start

Project uses PostgreSQL.

Create database with dedicated user.

Set credentials in application.yml file or as environment variables.
```
spring.datasource.url
spring.datasource.username
spring.datasource.password
```

Build application
```
./gradlew build
```

Run application
```
./gradlew bootRun
```

Application will start on port `8989`

SwaggerUI will be available here:
```
http://localhost:8989/swagger-ui/index.html#/prices-controller
```

Populate database:
```
curl --location --request GET 'http://localhost:8989/prices/mockMessage?message=106,%20EUR/USD,%201.1000,1.2000,01-06-2020%2012:01:01:001%0A107,%20EUR/JPY,%20119.60,119.90,01-06-2020%2012:01:02:002%0A108,%20GBP/USD,%201.2500,1.2560,01-06-2020%2012:01:02:002%0A109,%20GBP/USD,%201.2499,1.2561,01-06-2020%2012:01:02:100%0A110,%20EUR/JPY,%20119.61,119.91,01-06-2020%2012:01:02:110'
```

Get prices:
```
curl --location --request GET 'http://localhost:8989/prices'
```

Get price by instrument name, eg. GBP/USD:
```
curl --location --request GET 'http://localhost:8989/price?instrumentName=GBP/USD'
```
